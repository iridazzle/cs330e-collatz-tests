#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)
        
    def test_read_2(self):
        s = "100 200\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  100)
        self.assertEqual(j, 200)
    
    def test_read_3(self):
        s = "3 7\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  3)
        self.assertEqual(j, 7)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        memo = []
        for x in range(100000):
            memo.append(-1)
        v = collatz_eval(1, 10, memo)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        memo = []
        for x in range(100000):
            memo.append(-1)
        v = collatz_eval(100, 200, memo)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        memo = []
        for x in range(100000):
            memo.append(-1)
        v = collatz_eval(201, 210, memo)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        memo = []
        for x in range(100000):
            memo.append(-1)
        v = collatz_eval(900, 1000, memo)
        self.assertEqual(v, 174)
     
    def test_eval_5(self):
        memo = []
        for x in range(100000):
            memo.append(-1)
        v = collatz_eval(12, 14, memo)
        self.assertEqual(v, 18)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")
    
    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 100, 1000, 2000)
        self.assertEqual(w.getvalue(), "100 1000 2000\n")
    
    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 100, 1000, 2000)
        self.assertEqual(w.getvalue(), "100 1000 2000\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
    
    # Stress test 
    def test_solve_2(self):
        r = StringIO("2 10\n111 200\n201 210\n900 100000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "2 10 20\n111 200 125\n201 210 89\n900 100000 351\n")

    def test_solve_3(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
